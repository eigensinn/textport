﻿/*
 * Created by SharpDevelop.
 * User: drondin
 * Date: 28.01.2019
 * Time: 9:26
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Packaging;


namespace textport
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		void Button1Click(object sender, EventArgs e)
		{
			string path = this.textBox1.Text;
			var linesList = new List<string>();
			string slide_number_txt = this.tb_slidename.Text;
			int slide_number;
			if (path != "")
				if (slide_number_txt == "")
			    {
					slide_number_txt = "1";
			        this.tb_slidename.AppendText("1");
			    }
				slide_number = Int32.Parse(slide_number_txt);
				foreach (string s in GetAllTextInSlide(@path, slide_number-1))
					linesList.Add(s);
			    string[] lines = linesList.ToArray();
			    string filename = path.Split('\\').Last();
				File.WriteAllLines(@filename+"_"+slide_number_txt+".txt", lines);
		}
		void Btn_openClick(object sender, EventArgs e)
		{
			openFileDialog1.Filter = "MS PowerPoint files (*.pptx)|*.pptx|All files (*.*)|*.*";
	        if(openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)  
		    {
	        	this.textBox1.Clear();
		        string filename = openFileDialog1.FileName;
                this.textBox1.AppendText(filename);
		    }  
		}
		// Get all the text in a slide.
	    public static string[] GetAllTextInSlide(string presentationFile, int slideIndex)
	    {
	        // Open the presentation as read-only.
	        using (PresentationDocument presentationDocument = PresentationDocument.Open(presentationFile, false))
	        {
	            // Pass the presentation and the slide index
	            // to the next GetAllTextInSlide method, and
	            // then return the array of strings it returns. 
	            return GetAllTextInSlide(presentationDocument, slideIndex);
	        }
	    }
	    public static string[] GetAllTextInSlide(PresentationDocument presentationDocument, int slideIndex)
	    {
	        // Verify that the presentation document exists.
	        if (presentationDocument == null)
	        {
	            throw new ArgumentNullException("presentationDocument");
	        }
	
	        // Verify that the slide index is not out of range.
	        if (slideIndex < 0)
	        {
	            throw new ArgumentOutOfRangeException("slideIndex");
	        }
	
	        // Get the presentation part of the presentation document.
	        PresentationPart presentationPart = presentationDocument.PresentationPart;
	
	        // Verify that the presentation part and presentation exist.
	        if (presentationPart != null && presentationPart.Presentation != null)
	        {
	            // Get the Presentation object from the presentation part.
	            Presentation presentation = presentationPart.Presentation;
	
	            // Verify that the slide ID list exists.
	            if (presentation.SlideIdList != null)
	            {
	                // Get the collection of slide IDs from the slide ID list.
	                DocumentFormat.OpenXml.OpenXmlElementList slideIds = 
	                    presentation.SlideIdList.ChildElements;
	
	                // If the slide ID is in range...
	                if (slideIndex < slideIds.Count)
	                {
	                    // Get the relationship ID of the slide.
	                    string slidePartRelationshipId = (slideIds[slideIndex] as SlideId).RelationshipId;
	
	                    // Get the specified slide part from the relationship ID.
	                    SlidePart slidePart = 
	                        (SlidePart)presentationPart.GetPartById(slidePartRelationshipId);
	
	                    // Pass the slide part to the next method, and
	                    // then return the array of strings that method
	                    // returns to the previous method.
	                    return GetAllTextInSlide(slidePart);
	                }
	            }
	        }
	
	        // Else, return null.
	        return null;
	    }
	    public static string[] GetAllTextInSlide(SlidePart slidePart)
	    {
	        // Verify that the slide part exists.
	        if (slidePart == null)
	        {
	            throw new ArgumentNullException("slidePart");
	        }
	
	        // Create a new linked list of strings.
	        LinkedList<string> texts = new LinkedList<string>();
	
	        // If the slide exists...
	        if (slidePart.Slide != null)
	        {
	            // Iterate through all the paragraphs in the slide.
	            foreach (DocumentFormat.OpenXml.Drawing.Paragraph paragraph in 
	                slidePart.Slide.Descendants<DocumentFormat.OpenXml.Drawing.Paragraph>())
	            {
	                // Create a new string builder.                    
	                StringBuilder paragraphText = new StringBuilder();
	
	                // Iterate through the lines of the paragraph.
	                foreach (DocumentFormat.OpenXml.Drawing.Text text in 
	                    paragraph.Descendants<DocumentFormat.OpenXml.Drawing.Text>())
	                {
	                    // Append each line to the previous lines.
	                    paragraphText.Append(text.Text);
	                }
	
	                if (paragraphText.Length > 0)
	                {
	                    // Add each paragraph to the linked list.
	                    texts.AddLast(paragraphText.ToString());
	                }
	            }
	        }
	
	        if (texts.Count > 0)
	        {
	            // Return an array of strings.
	            return texts.ToArray();
	        }
	        else
	        {
	            return null;
	        }
	    }
		void Tb_slidenameTextChanged(object sender, EventArgs e)
		{
	
		}
	}
}
