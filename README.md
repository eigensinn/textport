# TextPort

## Get all the text from a slide in a Microsoft PowerPoint presentation (Open XML SDK)

The application is written in SharpDevelop 5.1 based on [a script in C#](https://docs.microsoft.com/ru-ru/office/open-xml/how-to-get-all-the-text-in-a-slide-in-a-presentation) using the Open XML SDK.

## Получить весь текст из слайда в презентации Microsoft PowerPoint (Open XML SDK)

Приложение написано в SharpDevelop 5.1 на основе [скрипта на C#](https://docs.microsoft.com/ru-ru/office/open-xml/how-to-get-all-the-text-in-a-slide-in-a-presentation), использующего Open XML SDK.

[Download textport.exe](https://yadi.sk/d/a6EFfiFLAIRkPA)

## License

[MIT License](https://bitbucket.org/eigensinn/textport/src/master/LICENSE)
